FROM node:14-alpine as build
WORKDIR /build
ARG BACKEND_URL
ENV NEXT_PUBLIC_BACKEND_URL $BACKEND_URL
COPY package.json ./
COPY yarn.lock .
RUN yarn install --frozen-lockfile
COPY . .
RUN yarn export

FROM nginx:stable-alpine
COPY --from=build /build/out /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
