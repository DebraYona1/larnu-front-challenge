import { Planet } from '@models/planet';
import { getPlanet } from 'api/planets';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useState } from 'react';

export const usePlanetFromQuery = () => {
  const [planet, setPlanet] = useState<Planet | null>(null);
  const [planetReady, setPlanetReady] = useState(false);
  const { query, isReady, push } = useRouter();

  const updatePlanet = useCallback(async (id: string) => {
    const planetData = await getPlanet(Number.parseInt(id, 10));
    setPlanet(planetData);
    setPlanetReady(true);
  }, []);

  useEffect(() => {
    if (isReady) {
      if ('id' in query && typeof query.id === 'string') {
        try {
          updatePlanet(query.id);
        } catch {
          push('/planets');
          setPlanetReady(true);
        }
      } else {
        setPlanetReady(true);
      }
    }
  }, [isReady]);

  return { planet, isReady: planetReady };
};
