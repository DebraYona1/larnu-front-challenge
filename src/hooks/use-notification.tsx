import { NotificationContext } from '@components/notifications/Notifications';
import { useContext } from 'react';

export const useNotification = () => {
  const { showNotification } = useContext(NotificationContext);
  return { showNotification };
};
