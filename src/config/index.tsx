export const getConfig = () => {
  const backendURL: string = process.env.NEXT_PUBLIC_BACKEND_URL ?? 'https://google.com';

  return {
    backendURL,
  };
};
