import React, { createContext, useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';
import { AnimatePresence, motion } from 'framer-motion';

export interface INotificationContext {
  showNotification: () => void;
}

export const NotificationContext = createContext<INotificationContext>({} as INotificationContext);

const NotificationColumn = styled.div`
  position: fixed;
  right: 0;
  top: 0;
  padding: 0.4rem;
  display: flex;
  flex-direction: column;
  align-items: end;
  width: 240px;
  pointer-events: none;
  & > * {
    pointer-events: auto;
  }
`;

const NotificationContainer = styled(motion.div)`
  width: 100%;
  padding: 0.5rem 1.2rem;
  border-radius: 16px;
  background-color: ${(props) => props.theme.colors.appErrorRed};
  & > * {
    margin: 0.35rem 0;
  }
`;

const NotificationTitle = styled.div`
  font-size: 16px;
  font-weight: 700;
`;
const NotificationMessage = styled.div`
  font-size: 14px;
  font-weight: 400;
`;

export const NotificationsProvider: React.FC = ({ children }) => {
  const [isNotificationVisible, setNotificationVisibility] = useState(false);

  const showNotification = useCallback(() => {
    setNotificationVisibility(false);
    setNotificationVisibility(true);
  }, []);

  // eslint-disable-next-line consistent-return
  useEffect(() => {
    if (isNotificationVisible) {
      const timer = setTimeout(() => {
        setNotificationVisibility(false);
      }, 3200);

      return () => {
        clearTimeout(timer);
      };
    }
  }, [isNotificationVisible]);

  return (
    <NotificationContext.Provider value={{ showNotification }}>
      <NotificationColumn>
        <AnimatePresence>
          {isNotificationVisible && (
            <NotificationContainer
              animate={{ scale: [0.25, 0.5, 1], y: ['-100%', '-50%', '0%'] }}
              transition={{ type: 'spring', duration: 0.5 }}
              exit={{ opacity: 0, x: ['0%', '50%', '100%'] }}>
              <NotificationTitle>ERROR:</NotificationTitle>
              <NotificationMessage>Ocurrió un problema en el servidor.</NotificationMessage>
            </NotificationContainer>
          )}
        </AnimatePresence>
      </NotificationColumn>
      {children}
    </NotificationContext.Provider>
  );
};
