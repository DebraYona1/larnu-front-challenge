import { Planet } from '@models/planet';
import { createPlanet, editPlanet } from 'api/planets';
import { convertFormDataToPlanet, convertPlanetToFormData } from 'helpers/transformers';
import { decimalValidator, emptyOrURLValidator, integerValidator } from 'helpers/validators';
import { useNotification } from 'hooks/use-notification';
import { useRouter } from 'next/router';
import { useCallback } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import styled from 'styled-components';

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border-radius: 16px;
  background-color: ${(props) => props.theme.colors.appLilac[600]};
  width: 100%;
  padding: 2rem 2.7rem;
  & > * {
    margin: 0.5rem 0;
  }
`;

const Label = styled.label`
  display: flex;
  width: 100%;
  flex-direction: column;
  font-size: 18px;
`;

const Input = styled.input`
  display: block;
  padding: 0.55rem 1.2rem;
  border-radius: 16px;
  overflow: hidden;
  border: 0px solid transparent;
  color: #000;
  appearance: textfield;
  margin: 0.3rem 0;
  &:focus-visible {
    outline: none;
    box-shadow: 0 0 3px 2px ${(props) => props.theme.colors.appLilac[300]};
  }
`;

const ErrorMessage = styled.span`
  color: ${(props) => props.theme.colors.appErrorRed};
  height: 24px;
  font-size: 14px;
`;

const SaveEditButton = styled.button`
  background-color: ${(props) => props.theme.colors.appBlue[500]};
  border-radius: 8px;
  border: 0;
  padding: 0.8rem 1.6rem;
  min-width: 120px;
  cursor: pointer;
  &:hover {
    background-color: ${(props) => props.theme.colors.appBlue[600]};
  }
`;

export interface PlanetFormInputs {
  name: string;
  satellites: string;
  diameter: string;
  imgUrl?: string;
}

interface PlanetFormProps {
  planet: Planet | null;
}

const PlanetForm: React.FC<PlanetFormProps> = ({ planet }) => {
  const { register, handleSubmit, errors } = useForm<PlanetFormInputs>(
    planet ? { defaultValues: convertPlanetToFormData(planet) } : undefined,
  );
  const { push } = useRouter();
  const { showNotification } = useNotification();

  const onSubmit = useCallback<SubmitHandler<PlanetFormInputs>>(async (data) => {
    try {
      await (planet
        ? editPlanet({ ...planet, ...convertFormDataToPlanet(data) })
        : createPlanet(convertFormDataToPlanet(data)));
      push('/planets');
    } catch {
      showNotification();
    }
  }, []);

  return (
    <FormContainer onSubmit={handleSubmit(onSubmit)}>
      <Label>
        Nombre:
        <Input name='name' ref={register({ required: true, maxLength: 30 })} />
        <ErrorMessage> {errors.name && 'Debe ingresar un nombre de máximo 30 caracteres'}</ErrorMessage>
      </Label>
      <Label>
        Número de Satélites:
        <Input name='satellites' ref={register({ validate: integerValidator })} />
        <ErrorMessage>{errors.satellites && errors.satellites.message}</ErrorMessage>
      </Label>
      <Label>
        {`Diámetro (km):`}
        <Input name='diameter' ref={register({ validate: decimalValidator })} />
        <ErrorMessage>{errors.diameter && errors.diameter.message}</ErrorMessage>
      </Label>
      <Label>
        {`URL de imagen (opcional)`}:
        <Input
          name='imgUrl'
          ref={register({
            validate: emptyOrURLValidator,
          })}
        />
        <ErrorMessage>{errors.imgUrl && errors.imgUrl.message}</ErrorMessage>
      </Label>
      <SaveEditButton type='submit'>{planet ? 'ACTUALIZAR' : 'CREAR'}</SaveEditButton>
    </FormContainer>
  );
};

export default PlanetForm;
