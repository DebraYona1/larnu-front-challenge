import { Planet } from '@models/planet';
import styled, { ThemeContext } from 'styled-components';
import { MdCreate, MdDelete } from 'react-icons/md';
import Link from 'next/link';
import { useCallback } from 'react';
import { deletePlanet } from 'api/planets';
import { useNotification } from 'hooks/use-notification';

interface PlanetCardProps {
  planet: Planet;
  refreshPlanets: () => void;
}

const Container = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 20px 240px;
  grid-gap: 1rem;
  background-color: ${(props) => props.theme.colors.appLilac[600]};
  border-radius: 16px;
  margin: auto;
  padding: 1rem 20px;
`;

const PlanetImage = styled.img`
  height: 60px;
  width: 60px;
  object-fit: cover;
  border-radius: 50%;
  overflow: hidden;
  margin: 0 -50px;
`;

const PlanetInfoContainer = styled.div`
  overflow: hidden;
`;

const PlanetFirstRow = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  flex-flow: nowrap;
`;

const PlanetNameText = styled.div`
  flex: 1;
  font-size: 24px;
  margin: 0.5rem 0;
`;

const EditButton = styled(MdCreate)`
  fill: ${(props) => props.theme.colors.appBlue[400]};
  border-radius: 50%;
  padding: 0.3rem;
  height: 30px;
  width: 30px;
  margin: 0.1rem;
  &:hover {
    cursor: pointer;
    background-color: ${(props) => props.theme.colors.appLilac[500]};
  }
`;

const DeleteButton = styled(MdDelete)`
  fill: ${(props) => props.theme.colors.appErrorRed};
  border-radius: 50%;
  padding: 0.3rem;
  height: 30px;
  width: 30px;
  margin: 0.1rem;
  &:hover {
    cursor: pointer;
    background-color: ${(props) => props.theme.colors.appLilac[500]};
  }
`;

const PlanetSatellitesText = styled.div`
  color: ${(props) => props.theme.colors.appLilac[300]};
  margin: 0.3rem 0;
`;
const PlanetDiameterText = styled.div`
  color: ${(props) => props.theme.colors.appLilac[300]};
  margin: 0.3rem 0;
`;

const PlanetCard: React.FC<PlanetCardProps> = ({ planet, refreshPlanets }) => {
  const { showNotification } = useNotification();
  const handleDelete = useCallback(async () => {
    try {
      await deletePlanet(planet);
      refreshPlanets();
    } catch {
      showNotification();
    }
  }, []);

  return (
    <Container>
      <PlanetImage
        src={
          planet.imgUrl ??
          'https://www.sciencemag.org/sites/default/files/styles/article_main_large/public/planet_1280p.jpg?itok=07tueTM9'
        }
      />
      <PlanetInfoContainer>
        <PlanetFirstRow>
          <PlanetNameText>{planet.name}</PlanetNameText>
          <Link href={planet.id ? `/planet?id=${planet.id}` : '/planets'}>
            <a>
              <EditButton />
            </a>
          </Link>
          <DeleteButton onClick={handleDelete} />
        </PlanetFirstRow>
        <PlanetSatellitesText>{`Satélites:  ${planet.satellites}`}</PlanetSatellitesText>
        <PlanetDiameterText>{`Diámetro:`}</PlanetDiameterText>
        <PlanetDiameterText>{`${planet.diameter} km`}</PlanetDiameterText>
      </PlanetInfoContainer>
    </Container>
  );
};

export default PlanetCard;
