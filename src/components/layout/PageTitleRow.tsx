import styled from 'styled-components';

export const PageTitleRow = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  padding: 1rem 2rem;
  & > * {
    margin: 0 0.8rem;
  }
`;
