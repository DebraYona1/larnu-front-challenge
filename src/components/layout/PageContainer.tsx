import styled from 'styled-components';

export const PageContainer = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: ${(props) => props.theme.colors.appLilac[400]};
`;
