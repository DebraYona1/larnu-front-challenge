import styled from 'styled-components';

export const PageTitle = styled.div`
  font-size: 64px;
  font-weight: bold;
`;
