export interface Planet {
  id?: number;
  name: string;
  satellites: number;
  diameter: number;
  imgUrl?: string;
}
