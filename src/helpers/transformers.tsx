import { PlanetFormInputs } from '@components/planets/PlanetForm';
import { Planet } from '@models/planet';

export const convertFormDataToPlanet = (data: PlanetFormInputs): Planet => {
  return {
    ...data,
    satellites: Number.parseInt(data.satellites, 10),
    diameter: Number.parseFloat(data.diameter),
  };
};

export const convertPlanetToFormData = (planet: Planet): PlanetFormInputs => {
  return {
    ...planet,
    satellites: planet.satellites.toString(),
    diameter: planet.diameter.toString(),
  };
};
