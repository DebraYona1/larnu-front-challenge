const isImageURLRegex = /^((https?|ftp):)?\/\/.*(jpeg|jpg|png|gif|bmp)$/i;
const isDecimalRegex = /^\d{1,4}(\.\d{1,3})?$/;
const isIntegerRegex = /^\d+$/;
const validateEmptyOrURL = (value: string) => {
  if (value === '') return true;
  return isImageURLRegex.test(value);
};

export const emptyOrURLValidator = (value: unknown) => {
  const stringToValidate = String(value);
  const isValid = validateEmptyOrURL(stringToValidate);

  return isValid || 'Debe ingresar una URL de imagen válida';
};

const validateDecimal = (value: string) => isDecimalRegex.test(value);

export const decimalValidator = (value: unknown) => {
  const stringToValidate = String(value);
  const isValid = validateDecimal(stringToValidate);

  return isValid || 'Debe ingresar un número decimal válido';
};

const validateInteger = (value: string) => isIntegerRegex.test(value);

export const integerValidator = (value: unknown) => {
  const stringToValidate = String(value);
  const isValid = validateInteger(stringToValidate);

  return isValid || 'Debe ingresar un número entero válido';
};
