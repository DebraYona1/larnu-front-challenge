export class APIException extends Error {
  constructor() {
    super('API Error');
  }
}
