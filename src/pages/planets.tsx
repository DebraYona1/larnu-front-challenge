import { GetServerSideProps, InferGetServerSidePropsType, NextPage } from 'next';
import { PageContainer } from '@components/layout/PageContainer';
import styled from 'styled-components';
import { Planet } from '@models/planet';
import PlanetCard from '@components/planets/PlanetCard';
import Link from 'next/link';
import React, { useCallback, useEffect, useState } from 'react';
import { PageTitle } from '@components/layout/PageTitle';
import { PageTitleRow } from '@components/layout/PageTitleRow';
import { getPlanets } from 'api/planets';
import { useNotification } from 'hooks/use-notification';

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
`;

const PlanetGrid = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 2rem;
  overflow-y: auto;
  margin: 1.2rem 0 0.4rem;
`;

const examplePlanet: Planet = {
  id: 121,
  name: 'Mercury',
  satellites: 1,
  diameter: 400322231231.12,
  imgUrl: 'https://www.sciencemag.org/sites/default/files/styles/article_main_large/public/planet_1280p.jpg',
};

const CreateButton = styled.button`
  background-color: ${(props) => props.theme.colors.appBlue[700]};
  border-radius: 8px;
  border: 0;
  margin: 0 3rem 0 auto;
  padding: 0.8rem 1.6rem;
  min-width: 120px;
  cursor: pointer;
  &:hover {
    background-color: ${(props) => props.theme.colors.appBlue[600]};
  }
`;

const PlanetListPage: NextPage = () => {
  const { showNotification } = useNotification();
  const [planets, setPlanets] = useState<Planet[]>([]);

  const refreshPlanets = useCallback(async () => {
    try {
      const newPlanets = await getPlanets();
      setPlanets(newPlanets);
    } catch {
      setPlanets([]);
      showNotification();
    }
  }, []);

  useEffect(() => {
    refreshPlanets();
  }, []);

  return (
    <PageContainer>
      <Container>
        <PageTitleRow>
          <PageTitle>Lista de Planetas</PageTitle>
        </PageTitleRow>
        <Link href='/planet'>
          <CreateButton>Añadir</CreateButton>
        </Link>
        <PlanetGrid>
          {planets.map((planet) => (
            <PlanetCard key={planet.id} planet={planet} refreshPlanets={refreshPlanets} />
          ))}
        </PlanetGrid>
      </Container>
    </PageContainer>
  );
};

export default PlanetListPage;
