import { PageContainer } from '@components/layout/PageContainer';
import { PageTitle } from '@components/layout/PageTitle';
import PlanetForm from '@components/planets/PlanetForm';
import { NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';
import { IoMdArrowRoundBack } from 'react-icons/io';
import { PageTitleRow } from '@components/layout/PageTitleRow';
import Link from 'next/link';
import { usePlanetFromQuery } from 'hooks/use-planet-from-query';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`;

const BackButton = styled(IoMdArrowRoundBack)`
  font-size: 64px;
  cursor: pointer;
`;

const FormRow = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  padding: 1rem 1.2rem;
`;

const FormContainer = styled.div`
  width: 100%;
  max-width: 600px;
  margin: 4rem 0 0 0;
`;

const PlanetPage: NextPage = () => {
  const { planet, isReady } = usePlanetFromQuery();

  return (
    <PageContainer>
      <Container>
        <PageTitleRow>
          <Link href='/planets'>
            <a>
              <BackButton />
            </a>
          </Link>
          <PageTitle>{planet ? 'Editar Planeta' : 'Crear Planeta'}</PageTitle>
        </PageTitleRow>
        <FormRow>
          <FormContainer>{isReady && <PlanetForm planet={planet} />}</FormContainer>
        </FormRow>
      </Container>
    </PageContainer>
  );
};

export default PlanetPage;
