import { NotificationsProvider } from '@components/notifications/Notifications';
import { AppProps } from 'next/app';
import React from 'react';
import { ThemeProvider } from 'styled-components';
import { GlobalStyle } from '../theme/global-styles';
import { theme } from '../theme/theme';

const App: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <NotificationsProvider>
        <Component {...pageProps} />
      </NotificationsProvider>
    </ThemeProvider>
  );
};

export default App;
