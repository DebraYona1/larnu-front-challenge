import { NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { PageContainer } from '@components/layout/PageContainer';

const Container = styled.div`
  height: 100%;
  width: 100%;
  position: relative;
  background-repeat: repeat-x;
  background-image: url('https://i.pinimg.com/originals/e3/c1/bf/e3c1bf71ae6e9aab1ee4326f1ed965b4.jpg');
  background-size: contain;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 8% 0;
  & > * {
    margin: 2.7rem 0;
  }
`;

const BlankSpace = styled.div`
  flex: 1;
`;

const EnterButton = styled.button`
  background-color: ${(props) => props.theme.colors.appLilac[400]};
  border-radius: 8px;
  border: 0;
  padding: 0.8rem 1.6rem;
  min-width: 120px;
  cursor: pointer;
  &:hover {
    background-color: ${(props) => props.theme.colors.appLilac[600]};
  }
`;

const AppTitle = styled.div`
  font-size: 5rem;
  font-weight: bold;
`;

const Home: NextPage = () => {
  return (
    <PageContainer>
      <Container>
        <BlankSpace />
        <AppTitle>PLANETARIO</AppTitle>
        <Link href='/planets'>
          <EnterButton>INGRESAR</EnterButton>
        </Link>
      </Container>
    </PageContainer>
  );
};

export default Home;
