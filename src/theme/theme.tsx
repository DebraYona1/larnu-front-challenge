import { DefaultTheme } from 'styled-components';

export const theme: DefaultTheme = {
  colors: {
    appLilac: {
      50: '#efedff',
      100: '#cfcaeb',
      200: '#aea9d7',
      300: '#8e87c6',
      400: '#6e64b4',
      500: '#544b9b',
      600: '#413a79',
      700: '#2f2a58',
      800: '#1b1837',
      900: '#090719',
    },
    appBlue: {
      50: '#daf5ff',
      100: '#ade1ff',
      200: '#7dd2ff',
      300: '#4dc6ff',
      400: '#24a6fe',
      500: '#1180e5',
      600: '#0055b3',
      700: '#003481',
      800: '#001a50',
      900: '#000520',
    },
    appErrorRed: '#ff695e',
  },
};
