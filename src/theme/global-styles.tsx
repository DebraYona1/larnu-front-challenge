import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';

export const GlobalStyle = createGlobalStyle`
  ${normalize}
  body, #__next {
    margin: 0;
    padding: 0;
    background-color: ${(props) => props.theme.colors.appLilac[400]};
  }
  * {
    color: #FFF;
    font-family: 'Varela Round', sans-serif;
    box-sizing: border-box;
  }
`;
