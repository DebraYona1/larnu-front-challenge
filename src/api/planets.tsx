import { Planet } from '@models/planet';
import { APIException } from 'errors';
import { baseAPI } from './baseapi';

export const getPlanets = async () => {
  try {
    const planets = await baseAPI.get<Planet[]>('/planets');

    if (planets.data) return planets.data;
  } catch (error) {
    console.error(error);
  }
  throw new APIException();
};

export const getPlanet = async (planetId: number) => {
  try {
    const planet = await baseAPI.get<Planet>(`/planets/${planetId}/`);

    if (planet.data) return planet.data;
  } catch (error) {
    console.error(error);
  }
  throw new APIException();
};

export const createPlanet = async (newPlanet: Planet) => {
  try {
    const planet = await baseAPI.post<Planet>('/planets/', newPlanet);

    if (planet.data) return planet.data;
  } catch (error) {
    console.error(error);
  }
  throw new APIException();
};

export const editPlanet = async (newPlanet: Planet) => {
  const { id, ...newPlanetData } = newPlanet;

  try {
    const planet = await baseAPI.patch<Planet>(`/planets/${id}/`, newPlanetData);

    if (planet.data) return planet.data;
  } catch (error) {
    console.error(error);
  }
  throw new APIException();
};

export const deletePlanet = async (newPlanet: Planet) => {
  try {
    const response = await baseAPI.delete<boolean>(`/planets/${newPlanet.id}/`);

    if (response.status === 204) return response.data;
  } catch (error) {
    console.error(error);
  }
  throw new APIException();
};
