import axios from 'axios';
import { getConfig } from 'config';

export const baseAPI = axios.create({
  baseURL: getConfig().backendURL,
});
