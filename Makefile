start:
	docker-compose up -d

logs: 
	docker-compose logs front

update:
	docker-compose down
	docker-compose build
	docker-compose up -d

stop:
	docker-compose down